﻿using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using System.IO;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

#if UNITY_CLOUD_BUILD
public class CloudBuilder : MonoBehaviour
{
    public static void PreExportAndroid( UnityEngine.CloudBuild.BuildManifestObject manifest)
    {
         Debug.Log("Abzorba Setting IL2CPP");
         PlayerSettings.SetScriptingBackend(EditorUserBuildSettings.selectedBuildTargetGroup, ScriptingImplementation.IL2CPP); 
         Debug.Log("Abzorba Setting ARM64|ARMv7");
         PlayerSettings.Android.targetArchitectures =  AndroidArchitecture.ARM64 | AndroidArchitecture.ARMv7;
    }
}
#endif

class AbzorbaCustomBuildProcessor : IPreprocessBuildWithReport, IPostprocessBuildWithReport
{
    public int callbackOrder { get { return 0; } }

    public void OnPreprocessBuild(BuildReport report)
    {
        Debug.Log($"{nameof(AbzorbaCustomBuildProcessor)}.{nameof(OnPreprocessBuild)} for target {report.summary.platform} at path {report.summary.outputPath}");
        // NO-OP
    }

    public void OnPostprocessBuild(BuildReport report)
    {
        Debug.Log($"{nameof(AbzorbaCustomBuildProcessor)}.{nameof(OnPostprocessBuild)} for target {report.summary.platform} at path {report.summary.outputPath}");
        EnablePushNotificationsForIOS(report.summary.outputPath);
        ExplainWhyXtremePushUsesBluetoothAndLocation(report.summary.outputPath);
    }

    private void ExplainWhyXtremePushUsesBluetoothAndLocation(string outputPath)
    {
        // Checkout Q & A here
        // https://support.xtremepush.com/hc/en-us/articles/360002262398-Get-Ready-for-iOS-13-Update-to-your-iOS-App-may-be-required-
#if UNITY_IOS
        Debug.Log($"{nameof(AbzorbaCustomBuildProcessor)}.{nameof(ExplainWhyXtremePushUsesBluetoothAndLocation)} - iOS - Setting bluetooth and location usage explaination START");
        // Read plist
        var plistPath = Path.Combine(outputPath, "Info.plist");
        var plist = new PlistDocument();
        plist.ReadFromFile(plistPath);

        // Update value
        PlistElementDict rootDict = plist.root;
        rootDict.SetString("NSLocationAlwaysUsageDescription", "XtremePush SDK requires Location - This Abzorba app does NOT make use of any location service");
        rootDict.SetString("NSLocationAlwaysAndWhenInUseUsageDescription", "XtremePush SDK requires Always in Use Location - This Abzorba app does NOT make use of any location service");
        rootDict.SetString("NSBluetoothPeripheralUsageDescription", "XtremePush SDK requires Bluetooth - This Abzorba app does NOT make use of any bluetooth service");

        // Write plist
        File.WriteAllText(plistPath, plist.WriteToString());
        Debug.Log($"{nameof(AbzorbaCustomBuildProcessor)}.{nameof(ExplainWhyXtremePushUsesBluetoothAndLocation)} - iOS - Setting bluetooth and location usage explaination SUCCESS");
#endif
    }

    private void EnablePushNotificationsForIOS(string path)
    {
#if UNITY_IOS
        Debug.Log($"{nameof(AbzorbaCustomBuildProcessor)}.{nameof(EnablePushNotificationsForIOS)} - iOS - Adding Push Notification capabilities START");

        // get XCode project path
        string pbxPath = PBXProject.GetPBXProjectPath(path);

        // Add linked frameworks
        PBXProject pbxProject = new PBXProject();
        pbxProject.ReadFromString(File.ReadAllText(pbxPath));
        string targetName = PBXProject.GetUnityTargetName();
        string targetGUID = pbxProject.TargetGuidByName(targetName);
        pbxProject.AddFrameworkToProject(targetGUID, "UserNotifications.framework", false);
        File.WriteAllText(pbxPath, pbxProject.WriteToString());

        // Add required capabilities: Push Notifications, and Remote Notifications in Background Modes
        var isDevelopment = Debug.isDebugBuild;
        var capabilities = new ProjectCapabilityManager(pbxPath, "app.entitlements", "Unity-iPhone");
        capabilities.AddPushNotifications(isDevelopment);
        capabilities.AddBackgroundModes(BackgroundModesOptions.RemoteNotifications);
        capabilities.WriteToFile();

        Debug.Log($"{nameof(AbzorbaCustomBuildProcessor)}.{nameof(EnablePushNotificationsForIOS)} - iOS - Adding Push Notification capabilities SUCCESS");
#endif
    }
}