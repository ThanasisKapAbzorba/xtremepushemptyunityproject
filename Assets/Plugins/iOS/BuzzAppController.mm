#import <UIKit/UIKit.h>
#import "UnityAppController.h"
#import "UI/UnityView.h"
#import "UI/UnityViewControllerBase.h"
#include "WebKit/WebKit.h"
#include <Foundation/Foundation.h>
#import <XPush/XPush.h>
#import <Firebase.h>

@interface BuzzAppController : UnityAppController <WKScriptMessageHandler>
{
    
}
- (void)createViewHierarchyImpl;
@end

@implementation BuzzAppController

- (void)willStartWithViewController:(UIViewController*)controller
{
    _unityView.contentScaleFactor   = UnityScreenScaleFactor([UIScreen mainScreen]);
    _unityView.autoresizingMask     = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _rootView= [[UIView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height)];
    [_rootView addSubview:_unityView];
    _rootController.view = _rootView;
}


- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message{
    if ([message.body isEqualToString:@"iOSInterface"]) {
        NSLog(@"iOS Interface");
    }
    if ([message.body isEqualToString:@"iOSExit"]) {
        NSLog(@"Send dismiss to view controller via delegate");
    }
    if ([message.name isEqualToString:@"percentage"]) {
        NSString *percentage = message.body;
    }
}

- (void) enableUnityView: (BOOL) enabled {
    [_unityView setUserInteractionEnabled:enabled];
}

// xtremepush START
// https://support.xtremepush.com/hc/en-us/articles/115004833265-Integrate-your-iOS-App-with-the-Platform-Objective-C-

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Is this needed ?
    // https://firebase.google.com/docs/ios/setup
    [FIRApp configure];

    bool father = [super application:application didFinishLaunchingWithOptions:launchOptions];

    // Key here is for staging, should be changed to production
    [XPush setAppKey: @"tCJcq5J_Ftx1gBpS7q6jYyT9AOThTy33"];
    [XPush registerForRemoteNotificationTypes:XPNotificationType_Alert | XPNotificationType_Sound | XPNotificationType_Badge];

    //    #if DEBUG
    //    [XPush setSandboxModeEnabled:YES];
    //    #endif
    // [XPush setShouldShowDebugLogs:YES];
    // [XPush setDebugModeEnabled:YES];

    [XPush applicationDidFinishLaunchingWithOptions:launchOptions];
    return father;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    [XPush applicationDidRegisterForRemoteNotificationsWithDeviceToken:deviceToken];


    [self callUnityObject:"AbzorbaApplication" Method:"XPushDeviceIDReceived" Parameter:"token"];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {

    [XPush applicationDidFailToRegisterForRemoteNotificationsWithError:error];

    [self callUnityObject:"AbzorbaApplication" Method:"XtremePushFailToRegister" Parameter:"error"];
}

- (void)application:(UIApplication *)application
    didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {

    [self callUnityObject:"AbzorbaApplication" Method:"XtremePushMessageResponseReceived" Parameter:"didReceiveRemoteNotification"];

    [XPush applicationDidReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}

- (void)application:(UIApplication *)application
    didReceiveLocalNotification:(UILocalNotification *)notification {

    [self callUnityObject:"AbzorbaApplication" Method:"XtremePushMessageResponseReceived" Parameter:"didReceiveLocalNotification"];

    [XPush applicationDidReceiveLocalNotification:notification];
}

- (void) application:(UIApplication *)application
    handleActionWithIdentifier:(NSString *)identifier
    forRemoteNotification:(NSDictionary *)userInfo
    completionHandler:(void (^)())completionHandler {

    [self callUnityObject:"AbzorbaApplication" Method:"XtremePushMessageResponseReceived" Parameter:"forRemoteNotification"];

    [XPush application:application
           handleActionWithIdentifier:identifier
           forRemoteNotification:userInfo
           completionHandler:completionHandler];
}

- (void) application:(UIApplication *)application
    handleActionWithIdentifier:(NSString *)identifier
    forLocalNotification:(UILocalNotification *)notification
    completionHandler:(void (^)())completionHandler {

    [self callUnityObject:"AbzorbaApplication" Method:"XtremePushMessageResponseReceived" Parameter:"forLocalNotification"];

    [XPush application:application
           handleActionWithIdentifier:identifier
           forLocalNotification:notification
           completionHandler:completionHandler];
}

// xtremepush END

- (void)callUnityObject:(const char*)object Method:(const char*)method Parameter:(const char*)parameter {
    UnitySendMessage(object, method, parameter);
}

#ifdef __cplusplus
extern "C" {
#endif
    
    
    
#ifdef __cplusplus
}
#endif
@end


@implementation ABZWebView
// ignore safe area insets for a true fullscreen expirience on devices with
// notches and end-to-end screens.
- (UIEdgeInsets)safeAreaInsets {
    return UIEdgeInsetsZero;
}

@end
IMPL_APP_CONTROLLER_SUBCLASS(BuzzAppController)
