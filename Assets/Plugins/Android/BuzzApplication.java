package buzzz.slots.casino;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import ie.imobile.extremepush.CredentialUpdateListener;
import ie.imobile.extremepush.MessageResponseListener;
import ie.imobile.extremepush.PushConnector;
import ie.imobile.extremepush.api.model.Message;

import java.util.HashMap;
import java.lang.ref.WeakReference;

import static ie.imobile.extremepush.PushConnector.mPushConnector;

public class BuzzApplication extends Application implements MessageResponseListener, CredentialUpdateListener  {

    @Override
    public void onCreate() {
        super.onCreate();
        // This now points to "Abzorba - Buzzslots - Staging" on xtremepush
        // Should be changed to point to Live/Staging when needed
        new PushConnector.Builder("ghMDVjL62NwCsVt3qWUMk-VJI3ZcT2dY", "1030584635685")
                .setTagsBatchingEnabled(true)
                .setMessageResponseListener(this)
                .setCredentialUpdateListener(this)
                .setShowForegroundNotifications(false) // https://support.xtremepush.com/hc/en-us/articles/115005187465-Updating-the-Android-SDK-
                .create(this);
    }

    @Override
    public void messageResponseReceived(Message message, HashMap<String, String> hashMap, WeakReference<Context> weakReference) {
        Log.d("Abzorba XPCallback", "messageResponseReceived" );
        UnityPlayer.UnitySendMessage("AbzorbaApplication", "XtremePushMessageResponseReceived", "dummy message");
    }

    @Override
    public void credentialUpdated(String credentialKey, String credentialValue) {
        if ("XPushDeviceID".compareTo(credentialKey) == 0)
        {
            sendXtremePushDeviceIdToUnity();
        }
    }

    private void sendXtremePushDeviceIdToUnity() {
        HashMap<String,String> deviceInfoMap = mPushConnector.getDeviceInfo(this);
        if (deviceInfoMap != null)
        {
            String XPushDeviceID = deviceInfoMap.get("XPushDeviceID");
            Log.d("Abzorba XPCallback", "XPushDeviceID : " + XPushDeviceID );
            UnityPlayer.UnitySendMessage("AbzorbaApplication", "XPushDeviceIDReceived", XPushDeviceID);
        }
    }
}
