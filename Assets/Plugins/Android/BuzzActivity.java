package buzzz.slots.casino;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;

import com.google.firebase.messaging.MessageForwardingService;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;


/* From the official Firebase plugin
We are using com.google.firebase.MessagingUnityPlayerActivity
https://firebase.google.com/docs/cloud-messaging/unity/client
but this might conflict when we introduce the Facebook SDK to the project.
*/

public class BuzzActivity extends UnityPlayerActivity {

    private static BuzzActivity mActivity;
    public CustomFrameLayout root;

/**
 * Workaround for when a message is sent containing both a Data and Notification payload.
 *
 * When the app is in the background, if a message with both a data and notification payload is
 * received the data payload is stored on the Intent passed to onNewIntent. By default, that
 * intent does not get set as the Intent that started the app, so when the app comes back online
 * it doesn't see a new FCM message to respond to. As a workaround, we override onNewIntent so
 * that it sends the intent to the MessageForwardingService which forwards the message to the
 * FirebaseMessagingService which in turn sends the message to the application.
 */
    @Override
    protected void onNewIntent(Intent intent) {
        Intent message = new Intent(this, MessageForwardingService.class);
        message.setAction(MessageForwardingService.ACTION_REMOTE_INTENT);
        message.putExtras(intent);
        message.setData(intent.getData());
        startService(message);
    }

/**
 * Dispose of the mUnityPlayer when restarting the app.
 *
 * This ensures that when the app starts up again it does not start with stale data.
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (mUnityPlayer != null) {
            mUnityPlayer.quit();
            mUnityPlayer = null;
        }

        mActivity = this;
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content);
        rootView.removeView(mUnityPlayer);
        root = new CustomFrameLayout(this);
        root.setBackgroundColor(getResources().getColor(android.R.color.black));
        root.addView(mUnityPlayer, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(root);
        mUnityPlayer.requestFocus();
    }
}
