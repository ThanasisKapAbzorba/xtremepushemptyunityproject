package buzzz.slots.casino;

import android.content.Context;

import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class CustomFrameLayout extends FrameLayout {

    public CustomFrameLayout(Context context) {
        super(context);
    }

    public CustomFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomFrameLayout( Context context, AttributeSet attrs,  int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
