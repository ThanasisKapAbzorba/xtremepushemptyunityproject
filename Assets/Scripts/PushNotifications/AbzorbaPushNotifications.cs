﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using CommonAssets.Scripts.General;
using UnityEngine;


namespace CommonAssets.Scripts.PushNotifications
{
    public class AbzorbaPushNotifications : UnitySingletonWithDontDestroyOnLoad<AbzorbaPushNotifications>
    {
        private string firebaseToken;
        private string xtremePushToken;

        protected override void SingletonAwake()
        {
            base.SingletonAwake();
            FirebaseInit();
        }

        // Start is called before the first frame update
        void FirebaseInit()
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.

                    // We need to get an instance of the app so that it is properly initialized
                    var app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.

                    LogFromConsole.AbzorbaDebug($"{nameof(AbzorbaPushNotifications)} {nameof(FirebaseInit)} success");

                    Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                    Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
                }
                else
                {
                    LogFromConsole.AbzorbaDebug(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }

        public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
        {
            LogFromConsole.AbzorbaDebug($"{nameof(AbzorbaPushNotifications)} Received Registration Token: {token.Token}");
            firebaseToken = token.Token;
        }

        public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
        {
            LogFromConsole.AbzorbaDebug($"{nameof(AbzorbaPushNotifications)} Received a new message : {e?.Message?.Notification?.Title}{Environment.NewLine}Message : {e?.Message?.Notification?.Body}{Environment.NewLine}Data : {string.Join(";", e.Message.Data.Select(x => string.Join("=", x.Key, x.Value)))}");
        }

        public void XtremePushMessageResponseReceived(string hashMapJsonString)
        {
            LogFromConsole.AbzorbaDebug($"{nameof(AbzorbaPushNotifications)} Received xtreme message");
        }

        public void XPushDeviceIDReceived(string XPushDeviceID)
        {
            LogFromConsole.AbzorbaDebug($"{nameof(AbzorbaPushNotifications)} Received Xtreme Registration Token: {XPushDeviceID}");
            xtremePushToken = XPushDeviceID;
        }

        public void XtremePushFailToRegister(string XPError)
        {
            LogFromConsole.AbzorbaDebug($"{nameof(AbzorbaPushNotifications)} Failed to register for Xtreme : {XPError}");
        }

        public string GetFirebaseToken()
        {
            return firebaseToken;
        }

        public string GetXtremePushToken()
        {
            return xtremePushToken;
        }
    }
}