﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonAssets.Scripts.General
{
    public class UnitySingletonWithDontDestroyOnLoad<T> : UnitySingleton<T> where T : MonoBehaviour
    {
        protected override void SingletonAwake()
        {
            base.SingletonAwake();
            DontDestroyOnLoad(this.gameObject);
        }
    }
}