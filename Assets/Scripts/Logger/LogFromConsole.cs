﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogFromConsole : MonoBehaviour
{
    private Text logText;

    static List<string> runInUpdate = new List<string>();

    void Awake()
    {
        logText = GetComponent<Text>();
    }

    public static void AbzorbaDebug(string logString)
    {
        Debug.Log(logString);
        lock (runInUpdate)
        {
            runInUpdate.Add(logString);
        }
    }

    private void Update()
    {
        lock (runInUpdate)
        {
            if (runInUpdate.Count > 0)
            {
                logText.text = string.Join(Environment.NewLine, runInUpdate) + Environment.NewLine + logText.text;
                runInUpdate.Clear();
            }
        }
    }
}