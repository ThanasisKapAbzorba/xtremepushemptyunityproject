﻿using System.Collections;
using System.Collections.Generic;
using CommonAssets.Scripts.PushNotifications;
using UnityEngine;

public class DisplayTokens : MonoBehaviour
{
    public void UserClickedToken()
    {
        LogFromConsole.AbzorbaDebug("Firebase Token : " + AbzorbaPushNotifications.Instance.GetFirebaseToken());
        LogFromConsole.AbzorbaDebug("XtremePush Token : " + AbzorbaPushNotifications.Instance.GetXtremePushToken());
    }
}
