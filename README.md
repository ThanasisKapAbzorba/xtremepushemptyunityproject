This is a sample unity empty project to test XtremePush SDK integration.
The SDK is imported as a native library for both iOS and Android.

**Libraries used**

* Firebase Analytics 6.6.0
* Firebase Messaging 6.6.0
* XtremePush Android 7.8.0
* XtremePush iOS 4.1.1

**Project description**

This is an empty scene unity project that simply displays tokens received on the Unity side.

XtremePush dependencies have been added using Jar Resolver (https://github.com/googlesamples/unity-jar-resolver) which is included with firebase.
There is a file *\Assets\XtremePush\Editor\XtremepushDependencies.xml* which installs the plugins automatically both on Gradle for Android and on iOS using CocoaPods.


In the *Assets\Plugins\Android\mainTemplate.gradle* file, the repo for XtremePush has been added.

**Goal of the sample**

We would like to have the Unity side of our project be notified when we receive the XtremePush token and the Xtreme (silent) Push notification.

This can be found in this file *Assets\Scripts\PushNotifications\AbzorbaPushNotifications.cs*
with the following functions

Firebase
* OnTokenReceived()
* OnMessageReceived()
    
XtremePush
* XPushDeviceIDReceived()
* XtremePushMessageResponseReceived()

These now work for android and we are getting the actual token but on iOS the token is **NULL**

**Continuous integration**

It is very important for us to be able to build directly from within Unity because we make use of Cloud Build systems.
Thus, it is very impractical to have permissions and frameworks changed manually after Unity project export.
So, changing a gradle build file or something in Xcode after unity build step is a no-go for Abzorba.

Therefore we have the *Assets\Editor\CloudBuilder.cs* file which serves as a continuous integration build process and enables the required permissions and frameworks for the iOs build.

**Success image**

A *SuccessImage/SuccessAndroid.png* file has been included which shows on Android how we are able to get the XtremePush token.
The iOS version now displays a dummy "token" string as the token comes in as null.